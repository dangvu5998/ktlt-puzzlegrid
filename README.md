# 2D grid word search puzzle - Program Technical Excercise

## Problem
Give one file contains words and one file contains grid characters m rows, n columns. Find all words appear in rows,
columns, diagonal in this grid

## Usage
Use compiler c++11 or newer to compile code with option -std=c++11, run main file, type your file name of dictionary and grid
This code doesn't work for older version of C++

## Build code
After clone code from repository, you need to compile code, get compiled file and run

    mkdir build
    g++ -std=c++11 src/*.cpp -o build/GridSolve

Then you can run compiled file:

    ./build/GridSolve
