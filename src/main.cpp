/*
 * main.cpp
 *
 *  Created on: Mar 17, 2018
 *      Author: trivu
 */
#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <algorithm>

#include "Grid.h"
#include "SolvePuzzle.h"
#include "WordInGrid.h"

#include <chrono>
#include "generate_test.h"
using namespace std;

// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

int main(){
//    generateGrid("test/grid200_200.txt", 200, 200);


//     caculating execution time
//    auto start = chrono::steady_clock::now();


    string dictPath;
    string gridPath;

//    dictPath = "test/dict.txt";
//    gridPath = "test/grid1000_1000.txt";

    cout << "Import grid file name:";
    cin >> gridPath;
    trim(dictPath);
    cout << "Import dictionary file name:";
    cin >> dictPath;
    trim(gridPath);
    Grid grid(gridPath);
    vector<string> dict = readDict(dictPath);
    vector<WordInGrid> result = solvePuzzle(grid, dict, 5);
    cout << grid.toString() << endl;
    cout << result.size() << endl;
    for(WordInGrid w: result){
        cout << w.toString() << endl;
    }


//     caculating execution time
//    auto end = chrono::steady_clock::now();
//    auto diff = end - start;
//    cout << "Duration time: "<< chrono::duration <double, milli> (diff).count() << " ms" << endl;
}



