/*
 * test.cpp
 *
 *  Created on: Mar 4, 2018
 *      Author: trivu
 */

#include <fstream>
using namespace std;

/*
 * Given file test path, grid size and generate test file
 *
 */
void generateGrid(string filePath, int m, int n, bool lowercase=true);


