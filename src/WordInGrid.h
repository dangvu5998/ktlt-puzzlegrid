/*
 * WordInGrid.h
 *
 *  Created on: Mar 17, 2018
 *      Author: trivu
 */
#include <string>
#ifndef WORDINGRID_H_
#define WORDINGRID_H_
class WordInGrid{
public:
    int x,y;
    std::string word;
    WordInGrid(string s, int i, int j){
        word = s;
        x = i;
        y = j;
    }

    string toString(){
        return word+" ["+to_string(x)+", "+to_string(y)+"]";
    }
};




#endif /* WORDINGRID_H_ */
