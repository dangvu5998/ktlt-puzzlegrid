/*
 * generate_test.cpp
 *
 *  Created on: Mar 17, 2018
 *      Author: trivu
 */
#include "generate_test.h"



void generateGrid(string filePath, int m, int n, bool lowercase){
    ofstream output(filePath);
    if(output.is_open()){
        output << m << " " << n << endl << endl;
        int firstChar = lowercase? 'a':'A';
        for(int i=0; i<m; i++){
            for(int j=0;j<n;j++){
                output << (char) (rand()%26 + firstChar) << " ";
            }
            output << endl;

        }
        output.close();
    }
    else{
        throw "File path error!";
    }
}

