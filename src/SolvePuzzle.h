/*
 * SolvePuzzle.h
 *
 *  Created on: Mar 5, 2018
 *      Author: trivu
 */
#include <string>

#include <vector>
#include <unordered_set>
#include "Grid.h"
#include "WordInGrid.h"
using namespace std;
#ifndef SOLVEPUZZLE_H_
#define SOLVEPUZZLE_H_

vector<string> readDict(string filePath);
vector<WordInGrid> solvePuzzle(Grid grid, vector<string> dictionary, int minLengthWord=0);
vector<WordInGrid> solvePuzzle(string gridPath, string dictPath, int minLengthWord=0);
#endif /* SOLVEPUZZLE_H_ */
