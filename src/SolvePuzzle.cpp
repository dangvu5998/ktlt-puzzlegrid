/*
 * SolvePuzzle.cpp
 *
 *  Created on: Mar 5, 2018
 *      Author: trivu
 */
#include <iostream>
#include <algorithm>
#include "SolvePuzzle.h"
#include <string>
inline string trim(string& str){
    str.erase(0, str.find_first_not_of(' '));       //prefixing spaces
    str.erase(str.find_last_not_of(' ')+1);         //surfixing spaces
    return str;
}

vector<string> readDict(string filePath){
    ifstream file(filePath);
    string word;
    vector<string> dict;
    if(file.is_open()){
        while(file.good() && file >> word){
            word = trim(word);
            if(word.size() > 0){
                transform(word.begin(), word.end(), word.begin(), ::tolower);
                dict.push_back(word);
            }
        }
    }
    return dict;
}
vector<WordInGrid> solvePuzzle(Grid grid, vector<string> dictionary, int minLengthWord){
    unordered_set<string> wordSet;
    int maxLengthWord = minLengthWord;
    for(string word: dictionary){
        if((int) word.size() > maxLengthWord) maxLengthWord = word.size();
        if((int) word.size() >= minLengthWord) wordSet.insert(word);
    }
    vector<WordInGrid> result;
    for(int i=0; i<grid.row; i++){
        for(int j=0; j<grid.col; j++){
            int jW = j, jE = j,
                iN = i, iS = i;
            string W = "", N = "", E = "", S = "",
                   WN = "", EN = "", WS = "", ES = "";
            for(int k=0; k<maxLengthWord; k++){
                if(grid.checkPosition(i,jW)){
                    W += grid.get(i,jW);
                    if(wordSet.count(W))
                        result.push_back(WordInGrid(W, i, j));
                }
                if(grid.checkPosition(iN,jW)){
                    WN += grid.get(iN,jW);
                    if(wordSet.count(WN))
                        result.push_back(WordInGrid(WN, i, j));
                }
                if(grid.checkPosition(iN,j)){

                    N += grid.get(iN, j);
                    if(wordSet.count(N)){
                        result.push_back(WordInGrid(N, i, j));
                    }
                }
                if(grid.checkPosition(iN,jE)){
                    EN += grid.get(iN, jE);
                    if(wordSet.count(EN))
                        result.push_back(WordInGrid(EN, i, j));
                }
                if(grid.checkPosition(i,jE)){
                    E += grid.get(i, jE);
                    if(wordSet.count(E))
                        result.push_back(WordInGrid(E, i, j));
                }
                if(grid.checkPosition(iS,jE)){
                    ES += grid.get(iS,jE);
                    if(wordSet.count(ES))
                        result.push_back(WordInGrid(ES, i, j));
                }
                if(grid.checkPosition(iS,j)){
                    S += grid.get(iS,j);
                    if(wordSet.count(S))
                        result.push_back(WordInGrid(S, i, j));
                }
                if(grid.checkPosition(iS,jW)){
                    WS += grid.get(iS,jW);
                    if(wordSet.count(WS))
                        result.push_back(WordInGrid(WS, i, j));
                }
                iN++; iS--; jW--; jE++;
            }
        }
    }
    return result;
}

vector<WordInGrid> solvePuzzle(string gridPath, string dictPath, int minLengthWord){
    return solvePuzzle(Grid(gridPath), readDict(dictPath), minLengthWord);
}



