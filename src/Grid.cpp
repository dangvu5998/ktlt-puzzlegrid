/*
 * Grid.cpp
 *
 *  Created on: Mar 17, 2018
 *      Author: trivu
 */
#include "Grid.h"
#include <iostream>
Grid::Grid(char **grid, int m, int n){
    this->grid = grid;
    row = m;
    col = n;
}
Grid::Grid(string filePath){
    ifstream file(filePath);
    if(file.is_open()){
        int m, n;
        char **grid;
        char c;
        file >> m >> n;
        grid = new char *[m];
        for(int i = 0; i<m; i++){
            grid[i] = new char[n];
            for(int j=0; j<n; j++){
                file >> c;
                grid[i][j] = tolower(c);
            }
        }
        this->grid = grid;
        row = m;
        col = n;
    }
    else {
        throw runtime_error("File not found!");
    }
}
bool Grid::checkPosition(int i, int j){
    return i >= 0 && i < row && j >= 0 && j < col;
}

char Grid::get(int i, int j){
    if(!checkPosition(i, j)){
        throw "out of range!";
    }
    return grid[i][j];
}

string Grid::toString(){
    string str = "";
    str += (to_string(row)+" "+to_string(col)+"\n\n");
    for(int i=0; i<row; i++){
        for(int j=0; j<col; j++){
            str+=grid[i][j];
            str+=" ";
        }
        str+="\n";
    }
    return str;
}
