/*
 * Grid.h
 *
 *  Created on: Mar 8, 2018
 *      Author: trivu
 */

#ifndef GRID_H_
#define GRID_H_
#include <string>
#include <stdexcept>
#include <fstream>
#include <ostream>

#include <ctype.h>

using namespace std;
class Grid{
public:
    int row, col;
    char **grid;
    Grid(char **grid, int m, int n);
    Grid(std::string filePath);
    bool checkPosition(int i, int j);
    char get(int i, int j);
    string toString();
};

#endif /* GRID_H_ */
